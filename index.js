const express = require('express')
const path = require('path')
const bodyParser = require('body-parser');
const Service = require('./server/service.js');

const PORT = process.env.PORT || 5000
const service = new Service();

express()
    .get('/', (req, res) => res.redirect("https://www.linkedin.com/in/forcey/"))
    .use('/layover', express.static(path.join(__dirname, 'public')))
    .use('/layover/query', bodyParser.urlencoded({ extended: true }))
    .post('/layover/query', service.query.bind(service))
    .listen(PORT, () => console.log(`Listening on ${PORT}`))
