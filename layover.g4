grammar layover;

query : location (flight location)+ EOF;

location : arrival=terminal stay=timespec? departure=terminal? ;
terminal : airports+=AIRPORT ('|' airports+=AIRPORT)* ;
timespec : d=DURATION
//		 | ArrivalTime DepartureTime
		 ;

/*
ArrivalTime   : 'Arrive before ' Time ;
DepartureTime : 'Departure after ' Time ;
Time 		  : MM/DD/YYYY HH:MM ('am' | 'pm') ;
*/

flight   : op=FLIGHT_OP c=carrier? ;
carrier  : '(' airlines+=AIRLINE ('|' airlines+=AIRLINE)* ')' ;

FLIGHT_OP : '-' '-'?                   ;
DURATION  : '+' [0-9]+ [hd]            ;
AIRPORT   : [A-Za-z] [A-Za-z] [A-Za-z] ;
AIRLINE   : [*A-Za-z0-9] [*A-Za-z0-9]  ;
WS 		  : [ \t\r\n]+ -> skip         ; // skip spaces, tabs, newlines
