# Layover #

Layover is a language for travelers to search for commericial air routes effeciently.

### Documentation ###

* [Pre-proposal doc](https://docs.google.com/document/d/1htEO1lKnVTT7HbiBVi02oZOkhJ77jKn4Q143pL7IalQ/edit#)
* [Preposal doc](https://docs.google.com/document/d/1TKOy5YtWVnxQm5qct1F90tdrk-TceRx1fq-F57hvbLg/edit#) - Language tutorial is here.
* [Design doc](https://docs.google.com/document/d/1YHBK7112HwGbphLUbl15IgpwPzMduLhwRjas6gm_Nfc/edit#heading=h.1tux3qwnyfva)
* [Report](https://docs.google.com/document/d/1gIoScklEaEu8DuYNn2CU9pyFQl9Pz33Le4MTnOzQWd0/edit?usp=sharing)

### Demo ###

The application is deployed at http://todd.li/layover. Here are some working examples:

Seattle to San Francisco, non-stop
```
SEA-SFO
```

Seattle to Beijing, allow connections in between
```
SEA--PEK
```

Multiple segments
```
SEA - SFO -- HKG
```

Specified airline
```
SEA -(AS) SFO --(JL|NH) HKG
```

Stay at a location
```
SEA -(AS) SFO +4d --(JL) HKG
```

Multiple departure/arrival airports
```
SEA|YVR -- KUL|SIN
```

Open-jaw
```
SEA-HND|NRT
KIX-TPE
```

### Setup ###

```
npm install
npm run postinstall		# will rebuild javascript files, useful if you change client/*.js.
npm start
```

### Data ###

Airline, airport and route data is kindly provided by [OpenFlights.org](https://openflights.org/data.html) 
under the [Open Database License](http://opendatacommons.org/licenses/odbl/1.0/).

### Owner ###

Todd Li (toddli@uw.edu)