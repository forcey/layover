const antlr4 = require("antlr4");
const moment = require("moment");
const LayoverLexer = require("../gen/layoverLexer").layoverLexer;
const LayoverParser = require("../gen/layoverParser").layoverParser;
const LayoverListener = require("../gen/layoverListener").layoverListener;
const ast = require('./ast.js');

class CompilerListener extends LayoverListener {
    constructor() {
        super();
    }

    enterQuery(ctx) {
        this.ast = [];
    }

    parseDuration(duration) {
        // duration is a single token like "+1d".
        var unit = duration.substr(-1, 1);
        var num = duration.substr(1, duration.length - 2);
        return moment.duration(parseInt(num), unit).asSeconds();
    }

    enterLocation(ctx) {
        var arrival = new Set(ctx.arrival.airports.map(token => token.text.toUpperCase()));
        var departure = arrival;
        if (ctx.departure) {
            departure = new Set(ctx.departure.airports.map(token => token.text.toUpperCase()));
        }
        var stay = 0;
        if (ctx.stay) {
            stay = this.parseDuration(ctx.stay.d.text);
        }
        this.ast.push(new ast.LocationSpec(arrival, departure, stay));
    }

    enterFlight(ctx) {
        var nonstop = (ctx.op.text == '-');
        var carrier = null;
        if (ctx.c) {
            var carrier = new Set(ctx.c.airlines.map(token => token.text.toUpperCase()));
        }
        this.ast.push(new ast.FlightSpec(carrier, nonstop));
    }
}

class LayoverErrorListener extends antlr4.error.ErrorListener {
    constructor() {
        super();
        this.errors = [];
    }

    syntaxError(recognizer, offendingSymbol, line, column, msg, e) {
        this.errors.push("line " + line + ":" + column + " " + msg);
    }
}

function lexAndParse(input, errorListener) {
    const chars = new antlr4.InputStream(input);
    const lexer = new LayoverLexer(chars);
    const tokens = new antlr4.CommonTokenStream(lexer);
    const parser = new LayoverParser(tokens);
    parser.buildParseTrees = true;
    parser.removeErrorListeners();
    parser.addErrorListener(errorListener);
    const tree = parser.query();
    return tree;
}

function compile(input) {
    const errorListener = new LayoverErrorListener();
    const tree = lexAndParse(input, errorListener);
    const compiler = new CompilerListener();
    antlr4.tree.ParseTreeWalker.DEFAULT.walk(compiler, tree);
    return {
        ast: compiler.ast,
        errors: errorListener.errors
    };
}

module.exports = compile;