// Defines classes used by the AST.

exports.LocationSpec = class LocationSpec {
    // arrival, departure: Set of airports
    // duration: number of seconds.
    constructor(arrival, departure, duration) {
        this.arrival = arrival;
        this.departure = departure;
        this.duration = duration;
    }
}

exports.FlightSpec = class FlightSpec {
    // carrier: Set of airlines
    // nonstop: boolean flag
    constructor(carrier, nonstop) {
        this.carrier = carrier;
        this.nonstop = nonstop;
    }
}

exports.FlightSegment = class FlightSegment {
    constructor(airline, flightNumber, origin, destination, departureTime, arrivalTime) {
        this.airline = airline; 
        this.flightNumber = flightNumber; 
        this.origin = origin; 
        this.destination = destination; 
        this.departureTime = departureTime; 
        this.arrivalTime = arrivalTime; 
    }
}

exports.Route = class Route {
    constructor(totalDuration, flights) {
        this.totalDuration = totalDuration;
        this.flights = flights;
    }
}