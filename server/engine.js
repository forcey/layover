// Execution engine.
const clone = require('clone');
const fs = require('fs');
const parse = require('csv-parse/lib/sync');
const FastPriorityQueue = require("fastpriorityqueue");
const greatCircle = require('great-circle')
const seedrandom = require('seedrandom');
const moment = require('moment-timezone');

const ast = require('./ast.js');

const MAX_RESULTS = 10;
const MIN_CONNECTION = 3600;

class Search {
    constructor(engine, query) {
        this.engine = engine;
        this.query = query;

        // Build a lookup of the minimum distance from any airport to any final destination.
        this.minimumDurationLookup = new Map();
        for (var origin of engine.airportsByIata.keys()) {
            var minimum = Infinity;
            for (var dest of query[query.length - 1].arrival) {
                minimum = Math.min(minimum, engine.minimumDuration(origin, dest));
            }
            if (minimum != Infinity) {
                this.minimumDurationLookup.set(origin, minimum);
            }
        }
    }

    minimumDuration(origin) {
        return this.minimumDurationLookup.get(origin) || 0;
    }
}

class SearchState {
    constructor(search) {
        this.search = search;

        this.route = new ast.Route(0, []);
        // Current location (airport code) after covered flight and ground segments.
        this.location = null;
        // Consumed query segments.
        this.consumed = 0;
    }

    clone() {
        var copy = new SearchState(this.search);

        copy.location = this.location;
        copy.consumed = this.consumed;
        copy.route = clone(this.route);

        return copy;
    }

    priority() {
        // Currently, the priority is based on elapsed time + minimum remaining duration as a heuristic.
        // In the future, consider weighing flight duration and ground duration differently.
        // (Time on the ground is less painful than on the flight.)
        return this.route.totalDuration + this.search.minimumDuration(this.location);
    }

    addFlight(flight) {
        this.route.flights.push(flight.toFlightSegment());
        this.route.totalDuration += flight.duration;
        this.location = flight.dest.iata;
    }
    
    addGround(newLocation, duration) {
        this.route.totalDuration += duration;
        this.location = newLocation;
    }
}

class Flight {
    constructor(airline, origin, dest, random) {
        this.airline = airline;
        this.origin = origin;
        this.dest = dest;

        // Distance in miles
        this.distance = greatCircle.distance(origin.lat, origin.long, dest.lat, dest.long, "MI");
        // OpenFlights hack - estimated duration is "30 min plus 1 hour per every 500 miles"
        this.duration = 1800 + 3600 * this.distance / 500;
        // A random flight number :)
        // The longer the route, the smaller the flight number tends to be.
        var min = Math.max(1, 1e6 / Math.pow(this.distance, 1.5));
        var max = Math.min(9999, min * 10);
        this.flightNumber = Math.floor(random() * (max - min) + min);

        // Random departure time within a day, in UTC seconds.
        this.departureTime = random() * 86400;
        this.arrivalTime = this.departureTime + this.duration;
    }

    toFlightSegment() {
        return new ast.FlightSegment(
            `${this.airline.name} (${this.airline.iata})`,
            this.flightNumber,
            `${this.origin.name} (${this.origin.iata})`,
            `${this.dest.name} (${this.dest.iata})`,
            moment.unix(this.departureTime).tz(this.origin.tz).format(),
            moment.unix(this.arrivalTime).tz(this.dest.tz).format()
        );
    }
}

module.exports = class Engine {
    constructor() {
        var airports = parse(fs.readFileSync("data/airports.dat"), {
            escape: '\\', auto_parse: true,
            columns: ['id', 'name', 'city', 'country', 'iata', null, 'lat', 'long', null, null, null, 'tz', null, null]
        });
        this.airportsByIata = new Map(airports.map(item => [item.iata, item]));
        this.airportsById = new Map(airports.map(item => [item.id, item]));

        var airlines = parse(fs.readFileSync("data/airlines.dat"), {
            escape: '\\', auto_parse: true,
            columns: ['id', 'name', 'alias', 'iata', null, null, null, 'active']
        });
        this.airlinesByIata = new Map(airlines.map(item => [item.iata, item]));
        this.airlinesById = new Map(airlines.map(item => [item.id, item]));

        var routes = parse(fs.readFileSync("data/routes.dat"), {
            escape: '\\', auto_parse: true,
            columns: ['airline', 'airlineId', 'origin', 'originId', 'dest', 'destId', 'codeshare', 'stops', 'vehicle']
        });
        this.flightsByOrigin = new Map();
        var random = seedrandom('layover');
        for (var route of routes) {
            if (route.codeshare == 'Y' || route.stops > 0) {
                // Don't include codeshare or direct flights.
                continue;
            }
            var airline = this.getAirline(route.airlineId, route.airline);
            var origin = this.getAirport(route.originId, route.origin);
            var dest = this.getAirport(route.destId, route.dest);
            if (!airline || !origin || !dest) {
                // Don't include routes to unknown airports.
                continue;
            }

            var flight = new Flight(airline, origin, dest, random);
            if (this.flightsByOrigin.has(origin.iata)) {
                this.flightsByOrigin.get(origin.iata).push(flight);
            } else {
                this.flightsByOrigin.set(origin.iata, [flight]);
            }
        }
    }

    minimumDuration(originAirport, destAirport) {
        // Estimate the minimum time to get from one airport to another. 
        // Easiest estimation is the great circle distance. In the future, consider floyd-warshall.
        var origin = this.getAirport(null, originAirport);
        var dest = this.getAirport(null, destAirport);

        if (origin && dest) {
            // Distance in miles
            var distance = greatCircle.distance(origin.lat, origin.long, dest.lat, dest.long, "MI");
            return 1800 + 3600 * distance / 500;
        } else {
            // No estimate available.
            return 0;
        }
    }

    getAirport(id, iata) {
        if (this.airportsById.has(id)) {
            return this.airportsById.get(id);
        }
        if (this.airportsByIata.has(iata)) {
            return this.airportsByIata.get(iata);
        }
    }

    getAirline(id, iata) {
        if (this.airlinesById.has(id)) {
            return this.airlinesById.get(id);
        }
        if (this.airlinesByIata.has(iata)) {
            return this.airlinesByIata.get(iata);
        }
    }

    validateAirports(airports, errors) {
        for (var airport of airports) {
            if (!this.getAirport(null, airport)) {
                var error = `Unknown airport ${airport}.`;
                if (!errors.includes(error)) {
                    errors.push(error);
                }
            }
        }
    }

    validateAirlines(airlines, errors) {
        for (var airline of airlines) {
            if (!this.getAirline(null, airline)) {
                var error = `Unknown airline ${airline}.`;
                if (!errors.includes(error)) {
                    errors.push(error);
                }
            }
        }
    }

    validate(query) {
        if (query.length == 0) {
            return ["Query is empty."];
        }
        var errors = [];
        for (var i = 0; i < query.length; i++) {
            if (i % 2 == 0) {
                if (!(query[i] instanceof ast.LocationSpec)) {
                    errors.push(`Query[${i}] should be a LocationSpec.`);
                }
                this.validateAirports(query[i].arrival, errors);
                this.validateAirports(query[i].departure, errors);
            } else {
                if (!(query[i] instanceof ast.FlightSpec)) {
                    errors.push(`Query[${i}] should be a FlightSpec.`);
                }
                if (query[i].carrier) {
                    this.validateAirlines(query[i].carrier, errors);
                }
            }
        }
        return errors;
    }

    fits(flight, flightSpec, locationSpec) {
        // Carrier check
        if (flightSpec.carrier && !flightSpec.carrier.has(flight.airline.iata)) {
            return false;
        }
        // Destination check
        if (flightSpec.nonstop && !locationSpec.arrival.has(flight.dest.iata)) {
            return false;
        }
        return true;
    }

    // query: an array of LocationSpecs interleaved with FlightSpec.
    // returns: an array of Routes
    execute(query) {
        var errors = this.validate(query);
        if (errors.length > 0) {
            return { routes: [], errors: errors };
        }

        var search = new Search(this, query);
        var queue = new FastPriorityQueue((a, b) => a.priority() < b.priority());
        for (var startLocation of query[0].departure) {
            var initialState = new SearchState(search);
            initialState.addGround(startLocation, 0);
            initialState.consumed = 1;
            queue.add(initialState);
        }
        var results = [];
        var counter = 0;
        while (!queue.isEmpty()) {
            var current = queue.poll();

            if (counter++ % 1024 == 0) {
                console.log(`Counter: ${counter}; Queue depth: ${queue.size}; Results: ${results.length}; Head duration: ${current.priority()}`);
            }
            if (counter > 5000) {
                return { routes: results, errors: ["Couldn't explore all possibilities."] };
            }

            if (current.consumed == query.length) {
                // Already at final destination!
                results.push(current.route);
                if (results.length == MAX_RESULTS) {
                    return { routes: results, errors: [] };
                }
                continue;
            }
            var flightSpec = query[current.consumed];
            var locationSpec = query[current.consumed + 1];
            var flights = this.flightsByOrigin.get(current.location);
            if (!flights) {
                continue;
            }
            for (var flight of flights) {
                // Can I fly this route?
                if (!this.fits(flight, flightSpec, locationSpec)) {
                    continue;
                }
                // Take this flight.
                var next = current.clone();
                next.addFlight(flight);
                // Have I reached my next destination?
                if (locationSpec.arrival.has(flight.dest.iata)) {
                    next.consumed += 2;

                    if (next.consumed == query.length) {
                        // Final destination, don't add stay time. But still put this node back to the queue for sorting.
                        queue.add(next);
                    } else {
                        // Location may have multiple departure options.
                        for (var departure of locationSpec.departure) {
                            var node = next.clone();
                            node.addGround(departure, Math.max(MIN_CONNECTION, locationSpec.duration));
                            queue.add(node);
                        }
                    }
                } else {
                    // Add minimum connection time.
                    next.addGround(flight.dest.iata, MIN_CONNECTION);
                    queue.add(next);
                }
            }
        }
        return { routes: results, errors: [] };
    }
}
