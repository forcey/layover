// The HTTP query handler.
const compile = require('./compiler.js');
const Engine = require('./engine.js');

module.exports = class Service {
    constructor() {
        this.engine = new Engine();
    }

    query(req, res) {
        var { ast, errors : compileErrors } = compile(req.body.query);
        console.log(ast);
        var { routes, errors: runtimeErrors } = this.engine.execute(ast);
        res.json({
            errors: compileErrors.concat(runtimeErrors),
            routes: routes
        });
    }
}