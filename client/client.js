const ejs = require('ejs');
const humanizeDuration = require('humanize-duration');

var resultsTemplate = ejs.compile($('#resultsTemplate').html());
var errorTemplate = ejs.compile($('#errorTemplate').html());

function onRunButton() {
    var result = $('#result');
    result.empty();
    $('#runButton').prop("disabled", true);

    $.post("query", { query: $('#queryBox').val() }, function (data) {
        result.append(resultsTemplate({
            routes: data.routes,
            errors: data.errors,
            humanizeDuration: humanizeDuration
        }));
    }, "json").fail(function (xhr, status, error) {
        result.append(errorTemplate({ error: xhr.responseText }));
    }).always(function () {
        $('#runButton').prop("disabled", false);
    });
}

$('#runButton').click(onRunButton);
