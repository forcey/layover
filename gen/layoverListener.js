// Generated from layover.g4 by ANTLR 4.7.1
// jshint ignore: start
var antlr4 = require('antlr4/index');

// This class defines a complete listener for a parse tree produced by layoverParser.
function layoverListener() {
	antlr4.tree.ParseTreeListener.call(this);
	return this;
}

layoverListener.prototype = Object.create(antlr4.tree.ParseTreeListener.prototype);
layoverListener.prototype.constructor = layoverListener;

// Enter a parse tree produced by layoverParser#query.
layoverListener.prototype.enterQuery = function(ctx) {
};

// Exit a parse tree produced by layoverParser#query.
layoverListener.prototype.exitQuery = function(ctx) {
};


// Enter a parse tree produced by layoverParser#location.
layoverListener.prototype.enterLocation = function(ctx) {
};

// Exit a parse tree produced by layoverParser#location.
layoverListener.prototype.exitLocation = function(ctx) {
};


// Enter a parse tree produced by layoverParser#terminal.
layoverListener.prototype.enterTerminal = function(ctx) {
};

// Exit a parse tree produced by layoverParser#terminal.
layoverListener.prototype.exitTerminal = function(ctx) {
};


// Enter a parse tree produced by layoverParser#timespec.
layoverListener.prototype.enterTimespec = function(ctx) {
};

// Exit a parse tree produced by layoverParser#timespec.
layoverListener.prototype.exitTimespec = function(ctx) {
};


// Enter a parse tree produced by layoverParser#flight.
layoverListener.prototype.enterFlight = function(ctx) {
};

// Exit a parse tree produced by layoverParser#flight.
layoverListener.prototype.exitFlight = function(ctx) {
};


// Enter a parse tree produced by layoverParser#carrier.
layoverListener.prototype.enterCarrier = function(ctx) {
};

// Exit a parse tree produced by layoverParser#carrier.
layoverListener.prototype.exitCarrier = function(ctx) {
};



exports.layoverListener = layoverListener;