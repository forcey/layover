// Generated from layover.g4 by ANTLR 4.7.1
// jshint ignore: start
var antlr4 = require('antlr4/index');


var serializedATN = ["\u0003\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964",
    "\u0002\n3\b\u0001\u0004\u0002\t\u0002\u0004\u0003\t\u0003\u0004\u0004",
    "\t\u0004\u0004\u0005\t\u0005\u0004\u0006\t\u0006\u0004\u0007\t\u0007",
    "\u0004\b\t\b\u0004\t\t\t\u0003\u0002\u0003\u0002\u0003\u0003\u0003\u0003",
    "\u0003\u0004\u0003\u0004\u0003\u0005\u0003\u0005\u0005\u0005\u001c\n",
    "\u0005\u0003\u0006\u0003\u0006\u0006\u0006 \n\u0006\r\u0006\u000e\u0006",
    "!\u0003\u0006\u0003\u0006\u0003\u0007\u0003\u0007\u0003\u0007\u0003",
    "\u0007\u0003\b\u0003\b\u0003\b\u0003\t\u0006\t.\n\t\r\t\u000e\t/\u0003",
    "\t\u0003\t\u0002\u0002\n\u0003\u0003\u0005\u0004\u0007\u0005\t\u0006",
    "\u000b\u0007\r\b\u000f\t\u0011\n\u0003\u0002\u0007\u0003\u00022;\u0004",
    "\u0002ffjj\u0004\u0002C\\c|\u0006\u0002,,2;C\\c|\u0005\u0002\u000b\f",
    "\u000f\u000f\"\"\u00025\u0002\u0003\u0003\u0002\u0002\u0002\u0002\u0005",
    "\u0003\u0002\u0002\u0002\u0002\u0007\u0003\u0002\u0002\u0002\u0002\t",
    "\u0003\u0002\u0002\u0002\u0002\u000b\u0003\u0002\u0002\u0002\u0002\r",
    "\u0003\u0002\u0002\u0002\u0002\u000f\u0003\u0002\u0002\u0002\u0002\u0011",
    "\u0003\u0002\u0002\u0002\u0003\u0013\u0003\u0002\u0002\u0002\u0005\u0015",
    "\u0003\u0002\u0002\u0002\u0007\u0017\u0003\u0002\u0002\u0002\t\u0019",
    "\u0003\u0002\u0002\u0002\u000b\u001d\u0003\u0002\u0002\u0002\r%\u0003",
    "\u0002\u0002\u0002\u000f)\u0003\u0002\u0002\u0002\u0011-\u0003\u0002",
    "\u0002\u0002\u0013\u0014\u0007~\u0002\u0002\u0014\u0004\u0003\u0002",
    "\u0002\u0002\u0015\u0016\u0007*\u0002\u0002\u0016\u0006\u0003\u0002",
    "\u0002\u0002\u0017\u0018\u0007+\u0002\u0002\u0018\b\u0003\u0002\u0002",
    "\u0002\u0019\u001b\u0007/\u0002\u0002\u001a\u001c\u0007/\u0002\u0002",
    "\u001b\u001a\u0003\u0002\u0002\u0002\u001b\u001c\u0003\u0002\u0002\u0002",
    "\u001c\n\u0003\u0002\u0002\u0002\u001d\u001f\u0007-\u0002\u0002\u001e",
    " \t\u0002\u0002\u0002\u001f\u001e\u0003\u0002\u0002\u0002 !\u0003\u0002",
    "\u0002\u0002!\u001f\u0003\u0002\u0002\u0002!\"\u0003\u0002\u0002\u0002",
    "\"#\u0003\u0002\u0002\u0002#$\t\u0003\u0002\u0002$\f\u0003\u0002\u0002",
    "\u0002%&\t\u0004\u0002\u0002&\'\t\u0004\u0002\u0002\'(\t\u0004\u0002",
    "\u0002(\u000e\u0003\u0002\u0002\u0002)*\t\u0005\u0002\u0002*+\t\u0005",
    "\u0002\u0002+\u0010\u0003\u0002\u0002\u0002,.\t\u0006\u0002\u0002-,",
    "\u0003\u0002\u0002\u0002./\u0003\u0002\u0002\u0002/-\u0003\u0002\u0002",
    "\u0002/0\u0003\u0002\u0002\u000201\u0003\u0002\u0002\u000212\b\t\u0002",
    "\u00022\u0012\u0003\u0002\u0002\u0002\u0006\u0002\u001b!/\u0003\b\u0002",
    "\u0002"].join("");


var atn = new antlr4.atn.ATNDeserializer().deserialize(serializedATN);

var decisionsToDFA = atn.decisionToState.map( function(ds, index) { return new antlr4.dfa.DFA(ds, index); });

function layoverLexer(input) {
	antlr4.Lexer.call(this, input);
    this._interp = new antlr4.atn.LexerATNSimulator(this, atn, decisionsToDFA, new antlr4.PredictionContextCache());
    return this;
}

layoverLexer.prototype = Object.create(antlr4.Lexer.prototype);
layoverLexer.prototype.constructor = layoverLexer;

Object.defineProperty(layoverLexer.prototype, "atn", {
        get : function() {
                return atn;
        }
});

layoverLexer.EOF = antlr4.Token.EOF;
layoverLexer.T__0 = 1;
layoverLexer.T__1 = 2;
layoverLexer.T__2 = 3;
layoverLexer.FLIGHT_OP = 4;
layoverLexer.DURATION = 5;
layoverLexer.AIRPORT = 6;
layoverLexer.AIRLINE = 7;
layoverLexer.WS = 8;

layoverLexer.prototype.channelNames = [ "DEFAULT_TOKEN_CHANNEL", "HIDDEN" ];

layoverLexer.prototype.modeNames = [ "DEFAULT_MODE" ];

layoverLexer.prototype.literalNames = [ null, "'|'", "'('", "')'" ];

layoverLexer.prototype.symbolicNames = [ null, null, null, null, "FLIGHT_OP", 
                                         "DURATION", "AIRPORT", "AIRLINE", 
                                         "WS" ];

layoverLexer.prototype.ruleNames = [ "T__0", "T__1", "T__2", "FLIGHT_OP", 
                                     "DURATION", "AIRPORT", "AIRLINE", "WS" ];

layoverLexer.prototype.grammarFileName = "layover.g4";



exports.layoverLexer = layoverLexer;

