// Generated from layover.g4 by ANTLR 4.7.1
// jshint ignore: start
var antlr4 = require('antlr4/index');
var layoverListener = require('./layoverListener').layoverListener;
var grammarFileName = "layover.g4";

var serializedATN = ["\u0003\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964",
    "\u0003\n9\u0004\u0002\t\u0002\u0004\u0003\t\u0003\u0004\u0004\t\u0004",
    "\u0004\u0005\t\u0005\u0004\u0006\t\u0006\u0004\u0007\t\u0007\u0003\u0002",
    "\u0003\u0002\u0003\u0002\u0003\u0002\u0006\u0002\u0013\n\u0002\r\u0002",
    "\u000e\u0002\u0014\u0003\u0002\u0003\u0002\u0003\u0003\u0003\u0003\u0005",
    "\u0003\u001b\n\u0003\u0003\u0003\u0005\u0003\u001e\n\u0003\u0003\u0004",
    "\u0003\u0004\u0003\u0004\u0007\u0004#\n\u0004\f\u0004\u000e\u0004&\u000b",
    "\u0004\u0003\u0005\u0003\u0005\u0003\u0006\u0003\u0006\u0005\u0006,",
    "\n\u0006\u0003\u0007\u0003\u0007\u0003\u0007\u0003\u0007\u0007\u0007",
    "2\n\u0007\f\u0007\u000e\u00075\u000b\u0007\u0003\u0007\u0003\u0007\u0003",
    "\u0007\u0002\u0002\b\u0002\u0004\u0006\b\n\f\u0002\u0002\u00028\u0002",
    "\u000e\u0003\u0002\u0002\u0002\u0004\u0018\u0003\u0002\u0002\u0002\u0006",
    "\u001f\u0003\u0002\u0002\u0002\b\'\u0003\u0002\u0002\u0002\n)\u0003",
    "\u0002\u0002\u0002\f-\u0003\u0002\u0002\u0002\u000e\u0012\u0005\u0004",
    "\u0003\u0002\u000f\u0010\u0005\n\u0006\u0002\u0010\u0011\u0005\u0004",
    "\u0003\u0002\u0011\u0013\u0003\u0002\u0002\u0002\u0012\u000f\u0003\u0002",
    "\u0002\u0002\u0013\u0014\u0003\u0002\u0002\u0002\u0014\u0012\u0003\u0002",
    "\u0002\u0002\u0014\u0015\u0003\u0002\u0002\u0002\u0015\u0016\u0003\u0002",
    "\u0002\u0002\u0016\u0017\u0007\u0002\u0002\u0003\u0017\u0003\u0003\u0002",
    "\u0002\u0002\u0018\u001a\u0005\u0006\u0004\u0002\u0019\u001b\u0005\b",
    "\u0005\u0002\u001a\u0019\u0003\u0002\u0002\u0002\u001a\u001b\u0003\u0002",
    "\u0002\u0002\u001b\u001d\u0003\u0002\u0002\u0002\u001c\u001e\u0005\u0006",
    "\u0004\u0002\u001d\u001c\u0003\u0002\u0002\u0002\u001d\u001e\u0003\u0002",
    "\u0002\u0002\u001e\u0005\u0003\u0002\u0002\u0002\u001f$\u0007\b\u0002",
    "\u0002 !\u0007\u0003\u0002\u0002!#\u0007\b\u0002\u0002\" \u0003\u0002",
    "\u0002\u0002#&\u0003\u0002\u0002\u0002$\"\u0003\u0002\u0002\u0002$%",
    "\u0003\u0002\u0002\u0002%\u0007\u0003\u0002\u0002\u0002&$\u0003\u0002",
    "\u0002\u0002\'(\u0007\u0007\u0002\u0002(\t\u0003\u0002\u0002\u0002)",
    "+\u0007\u0006\u0002\u0002*,\u0005\f\u0007\u0002+*\u0003\u0002\u0002",
    "\u0002+,\u0003\u0002\u0002\u0002,\u000b\u0003\u0002\u0002\u0002-.\u0007",
    "\u0004\u0002\u0002.3\u0007\t\u0002\u0002/0\u0007\u0003\u0002\u00020",
    "2\u0007\t\u0002\u00021/\u0003\u0002\u0002\u000225\u0003\u0002\u0002",
    "\u000231\u0003\u0002\u0002\u000234\u0003\u0002\u0002\u000246\u0003\u0002",
    "\u0002\u000253\u0003\u0002\u0002\u000267\u0007\u0005\u0002\u00027\r",
    "\u0003\u0002\u0002\u0002\b\u0014\u001a\u001d$+3"].join("");


var atn = new antlr4.atn.ATNDeserializer().deserialize(serializedATN);

var decisionsToDFA = atn.decisionToState.map( function(ds, index) { return new antlr4.dfa.DFA(ds, index); });

var sharedContextCache = new antlr4.PredictionContextCache();

var literalNames = [ null, "'|'", "'('", "')'" ];

var symbolicNames = [ null, null, null, null, "FLIGHT_OP", "DURATION", "AIRPORT", 
                      "AIRLINE", "WS" ];

var ruleNames =  [ "query", "location", "terminal", "timespec", "flight", 
                   "carrier" ];

function layoverParser (input) {
	antlr4.Parser.call(this, input);
    this._interp = new antlr4.atn.ParserATNSimulator(this, atn, decisionsToDFA, sharedContextCache);
    this.ruleNames = ruleNames;
    this.literalNames = literalNames;
    this.symbolicNames = symbolicNames;
    return this;
}

layoverParser.prototype = Object.create(antlr4.Parser.prototype);
layoverParser.prototype.constructor = layoverParser;

Object.defineProperty(layoverParser.prototype, "atn", {
	get : function() {
		return atn;
	}
});

layoverParser.EOF = antlr4.Token.EOF;
layoverParser.T__0 = 1;
layoverParser.T__1 = 2;
layoverParser.T__2 = 3;
layoverParser.FLIGHT_OP = 4;
layoverParser.DURATION = 5;
layoverParser.AIRPORT = 6;
layoverParser.AIRLINE = 7;
layoverParser.WS = 8;

layoverParser.RULE_query = 0;
layoverParser.RULE_location = 1;
layoverParser.RULE_terminal = 2;
layoverParser.RULE_timespec = 3;
layoverParser.RULE_flight = 4;
layoverParser.RULE_carrier = 5;

function QueryContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = layoverParser.RULE_query;
    return this;
}

QueryContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
QueryContext.prototype.constructor = QueryContext;

QueryContext.prototype.location = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(LocationContext);
    } else {
        return this.getTypedRuleContext(LocationContext,i);
    }
};

QueryContext.prototype.EOF = function() {
    return this.getToken(layoverParser.EOF, 0);
};

QueryContext.prototype.flight = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(FlightContext);
    } else {
        return this.getTypedRuleContext(FlightContext,i);
    }
};

QueryContext.prototype.enterRule = function(listener) {
    if(listener instanceof layoverListener ) {
        listener.enterQuery(this);
	}
};

QueryContext.prototype.exitRule = function(listener) {
    if(listener instanceof layoverListener ) {
        listener.exitQuery(this);
	}
};




layoverParser.QueryContext = QueryContext;

layoverParser.prototype.query = function() {

    var localctx = new QueryContext(this, this._ctx, this.state);
    this.enterRule(localctx, 0, layoverParser.RULE_query);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 12;
        this.location();
        this.state = 16; 
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        do {
            this.state = 13;
            this.flight();
            this.state = 14;
            this.location();
            this.state = 18; 
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        } while(_la===layoverParser.FLIGHT_OP);
        this.state = 20;
        this.match(layoverParser.EOF);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function LocationContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = layoverParser.RULE_location;
    this.arrival = null; // TerminalContext
    this.stay = null; // TimespecContext
    this.departure = null; // TerminalContext
    return this;
}

LocationContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
LocationContext.prototype.constructor = LocationContext;

LocationContext.prototype.terminal = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(TerminalContext);
    } else {
        return this.getTypedRuleContext(TerminalContext,i);
    }
};

LocationContext.prototype.timespec = function() {
    return this.getTypedRuleContext(TimespecContext,0);
};

LocationContext.prototype.enterRule = function(listener) {
    if(listener instanceof layoverListener ) {
        listener.enterLocation(this);
	}
};

LocationContext.prototype.exitRule = function(listener) {
    if(listener instanceof layoverListener ) {
        listener.exitLocation(this);
	}
};




layoverParser.LocationContext = LocationContext;

layoverParser.prototype.location = function() {

    var localctx = new LocationContext(this, this._ctx, this.state);
    this.enterRule(localctx, 2, layoverParser.RULE_location);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 22;
        localctx.arrival = this.terminal();
        this.state = 24;
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        if(_la===layoverParser.DURATION) {
            this.state = 23;
            localctx.stay = this.timespec();
        }

        this.state = 27;
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        if(_la===layoverParser.AIRPORT) {
            this.state = 26;
            localctx.departure = this.terminal();
        }

    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function TerminalContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = layoverParser.RULE_terminal;
    this._AIRPORT = null; // Token
    this.airports = []; // of Tokens
    return this;
}

TerminalContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
TerminalContext.prototype.constructor = TerminalContext;

TerminalContext.prototype.AIRPORT = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(layoverParser.AIRPORT);
    } else {
        return this.getToken(layoverParser.AIRPORT, i);
    }
};


TerminalContext.prototype.enterRule = function(listener) {
    if(listener instanceof layoverListener ) {
        listener.enterTerminal(this);
	}
};

TerminalContext.prototype.exitRule = function(listener) {
    if(listener instanceof layoverListener ) {
        listener.exitTerminal(this);
	}
};




layoverParser.TerminalContext = TerminalContext;

layoverParser.prototype.terminal = function() {

    var localctx = new TerminalContext(this, this._ctx, this.state);
    this.enterRule(localctx, 4, layoverParser.RULE_terminal);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 29;
        localctx._AIRPORT = this.match(layoverParser.AIRPORT);
        localctx.airports.push(localctx._AIRPORT);
        this.state = 34;
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        while(_la===layoverParser.T__0) {
            this.state = 30;
            this.match(layoverParser.T__0);
            this.state = 31;
            localctx._AIRPORT = this.match(layoverParser.AIRPORT);
            localctx.airports.push(localctx._AIRPORT);
            this.state = 36;
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        }
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function TimespecContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = layoverParser.RULE_timespec;
    this.d = null; // Token
    return this;
}

TimespecContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
TimespecContext.prototype.constructor = TimespecContext;

TimespecContext.prototype.DURATION = function() {
    return this.getToken(layoverParser.DURATION, 0);
};

TimespecContext.prototype.enterRule = function(listener) {
    if(listener instanceof layoverListener ) {
        listener.enterTimespec(this);
	}
};

TimespecContext.prototype.exitRule = function(listener) {
    if(listener instanceof layoverListener ) {
        listener.exitTimespec(this);
	}
};




layoverParser.TimespecContext = TimespecContext;

layoverParser.prototype.timespec = function() {

    var localctx = new TimespecContext(this, this._ctx, this.state);
    this.enterRule(localctx, 6, layoverParser.RULE_timespec);
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 37;
        localctx.d = this.match(layoverParser.DURATION);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function FlightContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = layoverParser.RULE_flight;
    this.op = null; // Token
    this.c = null; // CarrierContext
    return this;
}

FlightContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
FlightContext.prototype.constructor = FlightContext;

FlightContext.prototype.FLIGHT_OP = function() {
    return this.getToken(layoverParser.FLIGHT_OP, 0);
};

FlightContext.prototype.carrier = function() {
    return this.getTypedRuleContext(CarrierContext,0);
};

FlightContext.prototype.enterRule = function(listener) {
    if(listener instanceof layoverListener ) {
        listener.enterFlight(this);
	}
};

FlightContext.prototype.exitRule = function(listener) {
    if(listener instanceof layoverListener ) {
        listener.exitFlight(this);
	}
};




layoverParser.FlightContext = FlightContext;

layoverParser.prototype.flight = function() {

    var localctx = new FlightContext(this, this._ctx, this.state);
    this.enterRule(localctx, 8, layoverParser.RULE_flight);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 39;
        localctx.op = this.match(layoverParser.FLIGHT_OP);
        this.state = 41;
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        if(_la===layoverParser.T__1) {
            this.state = 40;
            localctx.c = this.carrier();
        }

    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function CarrierContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = layoverParser.RULE_carrier;
    this._AIRLINE = null; // Token
    this.airlines = []; // of Tokens
    return this;
}

CarrierContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
CarrierContext.prototype.constructor = CarrierContext;

CarrierContext.prototype.AIRLINE = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(layoverParser.AIRLINE);
    } else {
        return this.getToken(layoverParser.AIRLINE, i);
    }
};


CarrierContext.prototype.enterRule = function(listener) {
    if(listener instanceof layoverListener ) {
        listener.enterCarrier(this);
	}
};

CarrierContext.prototype.exitRule = function(listener) {
    if(listener instanceof layoverListener ) {
        listener.exitCarrier(this);
	}
};




layoverParser.CarrierContext = CarrierContext;

layoverParser.prototype.carrier = function() {

    var localctx = new CarrierContext(this, this._ctx, this.state);
    this.enterRule(localctx, 10, layoverParser.RULE_carrier);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 43;
        this.match(layoverParser.T__1);
        this.state = 44;
        localctx._AIRLINE = this.match(layoverParser.AIRLINE);
        localctx.airlines.push(localctx._AIRLINE);
        this.state = 49;
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        while(_la===layoverParser.T__0) {
            this.state = 45;
            this.match(layoverParser.T__0);
            this.state = 46;
            localctx._AIRLINE = this.match(layoverParser.AIRLINE);
            localctx.airlines.push(localctx._AIRLINE);
            this.state = 51;
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        }
        this.state = 52;
        this.match(layoverParser.T__2);
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


exports.layoverParser = layoverParser;
